<?php
/**
 * Created by PhpStorm.
 * User: bferrara
 * Date: 2016-06-13
 * Time: 9:47 AM
 */



// Create a simple matching game with randomized selection and adaptive 2nd-pick predictions (simple deduction)
// Run by console "php cardGameSimple.php"

Class cardGameSimple {

    private $computerPlayerMemory = [];
    private $gameScreen = '';
    private $gameOver = false;
    private $cardMap = [];
    private $currentlyFlipped = [];
    private $playerTurn = 'a';
    private $matches = [];
    private $turnFlips = [];
    private $messages = '';
    private $evenSquares = array(4, 16, 36, 64, 100);
    private $difficulties = ['Lame', 'Basic', 'Easy', 'Tricky', 'Tough'];
    private $cardLimit = 36;
    private $rowSize = 0;
    private $player = false;


    function __construct($cardMap = null)
    {
        if(is_null($cardMap)){
            $this->generateCardMap();
        }

    }

    private function generateCardMap(){
        $selectedCards = [];
        $possibleFaces = str_split("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");

        while ( count($selectedCards) < ($this->cardLimit/2) ){
            $randomChoice = $possibleFaces[rand(0, count($possibleFaces) - 1)];
            if(!in_array($randomChoice, $selectedCards)){
                //Do not allow repeated faces, I could build a config for this but its just a test.
                $selectedCards[] = $randomChoice;
            }
        }

        $finalMap = array_merge($selectedCards, $selectedCards);

        $this->cardMap = $finalMap;

        $this->verifyEvenSquare();
    }

    private function verifyEvenSquare(){

        if(count($this->cardMap) != $this->cardLimit){
            throw new Error("Failure to generate a card map with possible faces. (Card Map and Card Limit fo not match)");
        }

        $this->rowSize = sqrt(count($this->cardMap));

        if($this->rowSize - intval($this->rowSize) != 0){
            throw new Error("Failure to generate a card map with possible faces. (Should be a square)");
        }

        return true;
    }


    public function startScreen(){

        $input = '';

        while($input != 'Q'){

            echo "Command List: " .PHP_EOL
                ."S - Start Computer Simulation Mode" . PHP_EOL
                ."H - Start Human Interactive Mode" . PHP_EOL
                ."0 - Change Difficulty to Lame" .PHP_EOL
                ."1 - Change Difficulty to Basic" .PHP_EOL
                ."2 - Change Difficulty to Easy" .PHP_EOL
                ."3 - Change Difficulty to Tricky" .PHP_EOL
                ."4 - Change Difficulty to Tough" .PHP_EOL
                ."E - Edit Card Faces" .PHP_EOL
                ."Q - Quit" .PHP_EOL
                . $this->messages .PHP_EOL;



            $input = $this->humanInput();
            $this->messages = $input;
            switch($input){
                case 'S':
                    $this->player = false;
                    $this->gameStart();
                    break;
                case 'H':
                    $this->player = true;
                    $this->gameStart();
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                    $this->cardLimit = $this->evenSquares[intval($input)];
                    $this->generateCardMap();
                    $this->messages = "Difficulty changed to ".$this->difficulties[intval($input)];
                    break;
                case 'E':
                    $this->messages = "OOPs! They Didn't Pay Me Enough To Be This Adaptive.";
                    break;
                case 'Q':

                    break;
                default :
                    $this->messages = "Wrong input! (". $input .")";
            }

            system('clear');

        }

        echo "Thank you for playing...\n";
    }

    private function gameStart(){

        while (!$this->gameOver){

            echo $this->messages.PHP_EOL;
            $this->printGame();

            do{
                echo $this->messages;
                $input = null;
                if($this->player == false){
                    $input = $this->computerPlayer();
                }
                else{
                    $input = $this->humanInput();
                }
            }while(!$this->gameCommand($input));

            $this->gameLogic();

        }
        $this->gameOver = false;
        $this->currentlyFlipped = [];
        $this->computerPlayerMemory = [];
        $this->matches = [];
        $this->turnFlips = [];
        $this->playerTurn = 'a';

    }

    private function printGame(){
        system('clear');

        $this->messages = 'Q - Quit.'.PHP_EOL.'Make Card Selection:'.PHP_EOL;

        $this->gameScreen = '';
        $columnCounter = 0;

        if(!is_array($this->currentlyFlipped)){
            echo print_r($this->currentlyFlipped);
            die;
        }

        foreach ($this->cardMap as $key => $value){
            $this->gameScreen .= ' | '.$key." - [ ". (in_array($key, $this->currentlyFlipped) ? $value :  ' ' ) . " ]";
            if(++$columnCounter == $this->rowSize){
                $columnCounter = 0;
                $this->gameScreen .= PHP_EOL;
            }
        }

        echo $this->gameScreen;

    }

    private function gameCommand($inputChar){
        $this->messages = '';

        if($inputChar === 'Q'){
            $this->gameOver = true;
            return true;
        }

        if(intval($inputChar) < $this->cardLimit){
            if(in_array(intval($inputChar), $this->currentlyFlipped)){
                $this->messages = 'Already flipped, pick another.'.PHP_EOL;
                return false;
            }
            $this->turnFlips[] = intval($inputChar);
            $this->currentlyFlipped[] = intval($inputChar);
            return true;
        }

        return false;
    }

    private function gameLogic(){
        if($this->playerTurn == 'a'){

            //Humans are reading
            sleep(1);
            $this->playerTurn = 'b';
        }
        elseif($this->playerTurn == 'b'){

            $this->printGame();
            if($this->cardMap[$this->turnFlips[0]] == $this->cardMap[$this->turnFlips[1]]){
                $this->matches[$this->cardMap[$this->turnFlips[0]]] = $this->turnFlips;
                echo "Match Found :) WOOHOO!";
            }
            else{
                foreach ($this->turnFlips as $flipIndex){
                    if(($key = array_search($flipIndex, $this->currentlyFlipped)) !== false) {
                        unset($this->currentlyFlipped[$key]);
                    }
                }
                if(!isset($this->currentlyFlipped)){
                    $this->currentlyFlipped = [];
                }
                echo "No Match Found :( TRY AGAIN";
            }
            $this->turnFlips = [];
            $this->playerTurn = 'a';

            //Humans are reading
            sleep(1);
        }

    }

    private function computerPlayer(){

        //Humans are reading
        sleep(1);
        if(!empty($this->turnFlips)){
            //echo "in turn";
            if(isset($this->computerPlayerMemory[$this->cardMap[$this->turnFlips[0]]])){
                $rememberedCards = $this->computerPlayerMemory[$this->cardMap[$this->turnFlips[0]]];

                if(!empty($rememberedCards)){
                    if($rememberedCards[0] !== $this->turnFlips[0]){
                        return $rememberedCards[0];
                    }
                    if(isset($rememberedCards[1]) && $rememberedCards[1] !== $this->turnFlips[0]){
                        return $rememberedCards[1];
                    }
                }
            }
            $selection = $this->computerGuess();
            return $selection;
        }
        else{

            foreach ($this->computerPlayerMemory as $faceValue => $keys){
                if(count($keys) > 1){
                    if(!array_key_exists($faceValue, $this->matches)){
                        return $keys[0];
                    }
                }
            }
            $selection = $this->computerGuess();
            return $selection;
        }

    }

    private function computerGuess(){
        do{
            $selection = rand(0, count($this->cardMap) - 1);
            $memory = isset($this->computerPlayerMemory[$this->cardMap[$selection]]) ? $this->computerPlayerMemory[$this->cardMap[$selection]] : [];
        }while( in_array(intval($selection), $this->currentlyFlipped)
            || in_array(intval($selection), $this->turnFlips)
            || in_array(intval($selection), $memory));

        if(!isset($this->computerPlayerMemory[$this->cardMap[$selection]])){
            $this->computerPlayerMemory[$this->cardMap[$selection]] = [];
        }
        $this->computerPlayerMemory[$this->cardMap[$selection]][] = $selection;

        echo $selection;

        //Humans are reading
        sleep(1);
        return intval($selection);
    }

    private function humanInput(){

        $handle = fopen ("php://stdin","r");
        $input = fgets($handle);
        fclose($handle);
        return trim($input);
    }
}

$game = new cardGameSimple();
$game->startScreen();